-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema hotels
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema hotels
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `hotels` DEFAULT CHARACTER SET utf8 ;
USE `hotels` ;

-- -----------------------------------------------------
-- Table `hotels`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hotels`.`client` (
  `idClients` INT(11) NOT NULL,
  `clientName` VARCHAR(45) NULL DEFAULT NULL,
  `clientSureName` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `bankAccount` VARCHAR(45) NULL DEFAULT NULL,
  `clientState` BOOLEAN NULL DEFAULT NULL,
  PRIMARY KEY (`idClients`),
  UNIQUE INDEX `idClients_UNIQUE` (`idClients` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `hotels`.`client`(idClients,clientName,clientSureName,email,bankAccount,clientState)
VALUES (1,'Volodymyr','Mushynskyi','volodyq@gmail.com',1022559846,true),
(2,'Andryi','Mushynskyi','volodyq@gmail.com',1025559846,true),
(3,'Tom','Ryder','volodyq@gmail.com',1025759846,true);

-- -----------------------------------------------------
-- Table `hotels`.`hotel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hotels`.`hotel` (
  `idHotel` INT(11) NOT NULL AUTO_INCREMENT,
  `hotelName` VARCHAR(45) NULL DEFAULT NULL,
  `hotelAddress` VARCHAR(45) NULL DEFAULT NULL,
  `hotelsRooms` VARCHAR(45) NULL DEFAULT NULL,
  `hotelsFreeRoom` VARCHAR(45) NULL DEFAULT NULL,
  `booking_bookingHotelId` INT(11),
  PRIMARY KEY (`idHotel`),
  UNIQUE INDEX `idHotel_UNIQUE` (`idHotel` ASC),
  INDEX `fk_hotel_booking1_idx` (`booking_bookingHotelId` ASC),
  CONSTRAINT `fk_hotel_booking1`
    FOREIGN KEY (`booking_bookingHotelId`)
    REFERENCES `hotels`.`booking` (`bookingHotelId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `hotels`.`hotel`(idHotel,hotelName,hotelAddress,hotelsRooms,hotelsFreeRoom,booking_bookingHotelId)
VALUES (1,'Plassa','Kyiv,st. Franka 10','10','8',1),
(2,'Royal','Lviv, st. Shevchenks 13','5','5',null),
(3,'Tower','Herson, st. Bandery 5','5','4',3);

-- -----------------------------------------------------
-- Table `hotels`.`booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hotels`.`booking` (
  `idBooking` INT(11) NOT NULL AUTO_INCREMENT,
  `bookingInDate` DATETIME NULL DEFAULT NULL,
  `bookingOutDate` DATETIME NULL DEFAULT NULL,
  `client_idClients` INT(11) NOT NULL,
  `bookingHotelId` INT(11) NOT NULL,
  `bookingRoomId` INT(11) NOT NULL,
  PRIMARY KEY (`idBooking`, `bookingHotelId`, `bookingRoomId`),
  UNIQUE INDEX `idBooking_UNIQUE` (`idBooking` ASC),
  INDEX `fk_booking_client1_idx` (`client_idClients` ASC),
  CONSTRAINT `fk_booking_client1`
    FOREIGN KEY (`client_idClients`)
    REFERENCES `hotels`.`client` (`idClients`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `hotels`.`booking`(idBooking,bookingInDate,bookingOutDate,client_idClients,bookingHotelId,bookingRoomId)
VALUES (1,'20.10.19','25.10.19',1,1,7),
(2,'20.10.19','25.10.19',2,1,1),
(3,'20.10.19','25.10.19',3,3,3);
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Table `hotels`.`payment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hotels`.`payment` (
  `idPayment` INT(11) NOT NULL AUTO_INCREMENT,
  `paymentDate` DATETIME NULL DEFAULT NULL,
  `paymentSum` DOUBLE NULL DEFAULT NULL,
  `bankAccount` INT(11) NULL DEFAULT NULL,
  `paymentState` VARCHAR(45) NULL DEFAULT NULL,
  `booking_idBooking` INT(11) NOT NULL,
  PRIMARY KEY (`idPayment`),
  UNIQUE INDEX `idPayment_UNIQUE` (`idPayment` ASC),
  INDEX `fk_payment_booking1_idx` (`booking_idBooking` ASC),
  CONSTRAINT `fk_payment_booking1`
    FOREIGN KEY (`booking_idBooking`)
    REFERENCES `hotels`.`booking` (`idBooking`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `hotels`.`payment`(idPayment,paymentDate,paymentSum,bankAccount,paymentState,booking_idBooking)
VALUES (1,'20.10.19','2019',1027559846,'complete',1),
(2,'20.10.19','2019',1025559846,'complete',2),
(3,'20.10.19','2019',1025759846,'complete',3);
-- -----------------------------------------------------
-- Table `hotels`.`review`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hotels`.`review` (
  `idRoom` INT(11) NOT NULL,
  `clientReview` VARCHAR(45) NULL DEFAULT NULL,
  `starsReview` VARCHAR(45) NULL DEFAULT NULL,
  `dateReview` VARCHAR(45) NULL DEFAULT NULL,
  `client_idClients` INT(11) NOT NULL,
  PRIMARY KEY (`idRoom`),
  UNIQUE INDEX `idReview_UNIQUE` (`idRoom` ASC),
  INDEX `fk_review_client1_idx` (`client_idClients` ASC),
  CONSTRAINT `fk_review_client1`
    FOREIGN KEY (`client_idClients`)
    REFERENCES `hotels`.`client` (`idClients`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `hotels`.`review`(idRoom,clientReview,starsReview,dateReview,client_idClients)
VALUES (1,'Good hotel','*****','20.10.19',1);
-- -----------------------------------------------------
-- Table `hotels`.`room`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hotels`.`room` (
  `idRoom` INT(11) NOT NULL,
  `roomState` VARCHAR(45) NULL DEFAULT NULL,
  `roomPrice` DECIMAL(10,0) NULL DEFAULT NULL,
  `roomInDate` VARCHAR(45) NULL DEFAULT NULL,
  `roomOutDate`  VARCHAR(45) NULL DEFAULT NULL,
  `hotel_idHotel` INT(11) NOT NULL,
  `booking_bookingRoomId` INT(45) ,
  PRIMARY KEY (`idRoom`),
  INDEX `fk_room_hotel1_idx` (`hotel_idHotel` ASC),
  INDEX `fk_room_booking1_idx` (`booking_bookingRoomId` ASC),
  CONSTRAINT `fk_room_hotel1`
    FOREIGN KEY (`hotel_idHotel`)
    REFERENCES `hotels`.`hotel` (`idHotel`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_room_booking1`
    FOREIGN KEY (`booking_bookingRoomId`)
    REFERENCES `hotels`.`booking` (`bookingRoomId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `hotels`.`room`(idRoom,roomState,roomPrice,roomInDate,roomOutDate,hotel_idHotel,booking_bookingRoomId)
VALUES (1,'Fill',2019,'20.10.19','25.10.19',1,1),
(2,'Fill',2019,'22.10.19','23.10.19',1,2),
(3,'Free',2019,'','',1,null),
(4,'Free',2019,'','',1,null),
(5,'Free',2019,'','',1,null),
(6,'Free',2019,'','',1,null),
(7,'Free',2019,'','',1,null),
(8,'Free',2019,'','',1,null),
(9,'Free',2019,'','',1,null),
(10,'Free',2019,'','',1,null),
(1,'Free',2019,'','',2,null),
(2,'Free',2019,'','',2,null),
(3,'Free',2019,'','',2,null),
(4,'Free',2019,'','',2,null),
(5,'Free',2019,'','',2,null),
(1,'Fill',2019,'22.10.19','23.10.19',3,3),
(2,'Free',2019,'','',3,null),
(3,'Free',2019,'','',3,null),
(4,'Free',2019,'','',3,null),
(5,'Free',2019,'','',3,null);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

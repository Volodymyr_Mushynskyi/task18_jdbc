package com.epam.model;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;

@Table(name = "Booking")
public class Booking {
  @PrimaryKey
  @Column(name = "idBooking")
  private Integer idBooking;

  @PrimaryKey
  @Column(name = "hotelId")
  private Integer hotelId;

  @PrimaryKey
  @Column(name = "bookingInDate", length = 45)
  private String bookingInDate;

  @PrimaryKey
  @Column(name = "bookingOutDate", length = 45)
  private String bookingOutDate;

  @PrimaryKey
  @Column(name = "roomId", length = 45)
  private String roomId;

  public Booking() {
  }

  public Booking(Integer idBooking, Integer hotelId, String bookingInDate, String bookingOutDate, String roomId) {
    this.idBooking = idBooking;
    this.hotelId = hotelId;
    this.bookingInDate = bookingInDate;
    this.bookingOutDate = bookingOutDate;
    this.roomId = roomId;
  }

  public Integer getIdBooking() {
    return idBooking;
  }

  public void setIdBooking(Integer idBooking) {
    this.idBooking = idBooking;
  }

  public Integer getHotelId() {
    return hotelId;
  }

  public void setHotelId(Integer hotelId) {
    this.hotelId = hotelId;
  }

  public String getBookingInDate() {
    return bookingInDate;
  }

  public void setBookingInDate(String bookingInDate) {
    this.bookingInDate = bookingInDate;
  }

  public String getBookingOutDate() {
    return bookingOutDate;
  }

  public void setBookingOutDate(String bookingOutDate) {
    this.bookingOutDate = bookingOutDate;
  }

  public String getRoomId() {
    return roomId;
  }

  public void setRoomId(String roomId) {
    this.roomId = roomId;
  }
}

package com.epam.model;


import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;

@Table(name = "review")
public class Review {
  @PrimaryKey
  @Column(name = "idReview")
  private Integer idReview;
  @Column(name = "clientReview", length = 45)
  private String dateReview;
  @Column(name = "starsReview", length = 45)
  private String stars;
  @Column(name = "client_idClients")
  private Integer paymantState;

  public Review() {
  }

  public Review(Integer idReview, String dateReview, String stars, Integer paymantState) {
    this.idReview = idReview;
    this.dateReview = dateReview;
    this.stars = stars;
    this.paymantState = paymantState;
  }

  public Integer getIdReview() {
    return idReview;
  }

  public void setIdReview(Integer idReview) {
    this.idReview = idReview;
  }

  public String getDateReview() {
    return dateReview;
  }

  public void setDateReview(String dateReview) {
    this.dateReview = dateReview;
  }

  public String getStars() {
    return stars;
  }

  public void setStars(String stars) {
    this.stars = stars;
  }

  public Integer getPaymantState() {
    return paymantState;
  }

  public void setPaymantState(Integer paymantState) {
    this.paymantState = paymantState;
  }
}

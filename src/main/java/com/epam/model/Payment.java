package com.epam.model;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;

@Table(name = "payment")
public class Payment {
  @PrimaryKey
  @Column(name = "idPayment")
  private Integer id;
  @Column(name = "paymentDate", length = 45)
  private String date;
  @Column(name = "paymentSum")
  private Double sum;
  @Column(name = "bankAccount")
  private Integer account;
  @Column(name = "paymentState")
  private Boolean paymantState;
  @Column(name = "booking_bookingHotelId")
  private Integer state;

  public Payment() {
  }

  public Payment(Integer id, String date, Double sum, Integer account, Boolean paymantState, Integer state) {
    this.id = id;
    this.date = date;
    this.sum = sum;
    this.account = account;
    this.paymantState = paymantState;
    this.state = state;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public Double getSum() {
    return sum;
  }

  public void setSum(Double sum) {
    this.sum = sum;
  }

  public Integer getAccount() {
    return account;
  }

  public void setAccount(Integer account) {
    this.account = account;
  }

  public Boolean getPaymantState() {
    return paymantState;
  }

  public void setPaymantState(Boolean paymantState) {
    this.paymantState = paymantState;
  }

  public Integer getState() {
    return state;
  }

  public void setState(Integer state) {
    this.state = state;
  }
}

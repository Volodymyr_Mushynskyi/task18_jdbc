package com.epam.model;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;

@Table(name = "client")
public class Client {

    @PrimaryKey
    @Column(name = "idClients")
    private Integer id;
    @Column(name = "clientName", length = 45)
    private String name;
    @Column(name = "clientSureName", length = 45)
    private String surname;
    @Column(name = "email", length = 45)
    private String email;
    @Column(name = "bankAccount")
    private Integer account;
    @Column(name = "clientState", length = 45)
    private String state;

  public Client(){}

  public Client(Integer id, String name, String surname, String email, Integer account, String state) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.account = account;
    this.state = state;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Integer getAccount() {
    return account;
  }

  public void setAccount(Integer account) {
    this.account = account;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }
}

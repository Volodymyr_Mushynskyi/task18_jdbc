package com.epam.model;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;

@Table(name = "hotel")
public class Hotel {
  @PrimaryKey
  @Column(name = "idHotel")
  private Integer id;
  @Column(name = "hotelName", length = 45)
  private String hotelName;
  @Column(name = "hotelAddress", length = 45)
  private String hotelAddress;
  @Column(name = "hotelsRooms")
  private Integer hotelsRoom;
  @Column(name = "hotelsFreeRoom")
  private Integer freeRomm;
  @Column(name = "booking_bookingHotelId")
  private Integer bookingHotelId;

  public Hotel() {
  }

  public Hotel(Integer id, String hotelName, String hotelAddress, Integer hotelsRoom, Integer freeRomm, Integer bookingHotelId) {
    this.id = id;
    this.hotelName = hotelName;
    this.hotelAddress = hotelAddress;
    this.hotelsRoom = hotelsRoom;
    this.freeRomm = freeRomm;
    this.bookingHotelId = bookingHotelId;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getHotelName() {
    return hotelName;
  }

  public void setHotelName(String hotelName) {
    this.hotelName = hotelName;
  }

  public String getHotelAddress() {
    return hotelAddress;
  }

  public void setHotelAddress(String hotelAddress) {
    this.hotelAddress = hotelAddress;
  }

  public Integer getHotelsRoom() {
    return hotelsRoom;
  }

  public void setHotelsRoom(Integer hotelsRoom) {
    this.hotelsRoom = hotelsRoom;
  }

  public Integer getFreeRomm() {
    return freeRomm;
  }

  public void setFreeRomm(Integer freeRomm) {
    this.freeRomm = freeRomm;
  }

  public Integer getBookingHotelId() {
    return bookingHotelId;
  }

  public void setBookingHotelId(Integer bookingHotelId) {
    this.bookingHotelId = bookingHotelId;
  }
}

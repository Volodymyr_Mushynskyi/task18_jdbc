package com.epam.model;

import com.epam.model.annotation.Column;
import com.epam.model.annotation.PrimaryKey;
import com.epam.model.annotation.Table;

@Table(name = "room")
public class Room {
  @PrimaryKey
  @Column(name = "idRoom")
  private Integer idRoom;
  @Column(name = "roomState")
  private Boolean roomState;
  @Column(name = "roomPrice")
  private Double roomPrice;
  @Column(name = "roomInDate", length = 45)
  private String roomInDate;
  @Column(name = "roomOutDate", length = 45)
  private String roomOutDate;
  @Column(name = "hotel_idHotel", length = 45)
  private String hotelIdHotel;
  @Column(name = "bookingRoomId", length = 45)
  private String bookingRoomId;

  public Room() {
  }

  public Room(Integer idRoom, Boolean roomState, Double roomPrice, String roomInDate, String roomOutDate, String hotelIdHotel, String bookingRoomId) {
    this.idRoom = idRoom;
    this.roomState = roomState;
    this.roomPrice = roomPrice;
    this.roomInDate = roomInDate;
    this.roomOutDate = roomOutDate;
    this.hotelIdHotel = hotelIdHotel;
    this.bookingRoomId = bookingRoomId;
  }

  public Integer getIdRoom() {
    return idRoom;
  }

  public void setIdRoom(Integer idRoom) {
    this.idRoom = idRoom;
  }

  public Boolean getRoomState() {
    return roomState;
  }

  public void setRoomState(Boolean roomState) {
    this.roomState = roomState;
  }

  public Double getRoomPrice() {
    return roomPrice;
  }

  public void setRoomPrice(Double roomPrice) {
    this.roomPrice = roomPrice;
  }

  public String getRoomInDate() {
    return roomInDate;
  }

  public void setRoomInDate(String roomInDate) {
    this.roomInDate = roomInDate;
  }

  public String getRoomOutDate() {
    return roomOutDate;
  }

  public void setRoomOutDate(String roomOutDate) {
    this.roomOutDate = roomOutDate;
  }

  public String getHotelIdHotel() {
    return hotelIdHotel;
  }

  public void setHotelIdHotel(String hotelIdHotel) {
    this.hotelIdHotel = hotelIdHotel;
  }

  public String getBookingRoomId() {
    return bookingRoomId;
  }

  public void setBookingRoomId(String bookingRoomId) {
    this.bookingRoomId = bookingRoomId;
  }
}

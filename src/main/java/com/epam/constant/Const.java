package com.epam.constant;

public class Const {
  public static final String CREATE_CLIENT = "INSERT INTO client(idClients, clientName, clientSureName, email, bankAccount, clientState) VALUES (?, ?, ?, ?, ?, ?)";
  public static final String CREATE_HOTEL = "INSERT INTO hotel(idHotel, hotelName, hotelAddress, hotelsRooms, hotelsFreeRoom, booking_bookingHotelId) VALUES (?, ?, ?, ?, ?, ?)";
  public static final String CREATE_PAYMENT = "INSERT INTO payment(idPayment, paymentDate, paymentSum, bankAccount, paymentState, booking_bookingHotelId) VALUES (?, ?, ?, ?, ?, ?)";
  public static final String CREATE_REVIEW = "INSERT INTO review(idReview, clientReview, starsReview, client_idClients) VALUES (?, ?, ?, ?)";

  public static final String FIND_ALL_CLIENTS = "SELECT * FROM client";
  public static final String FIND_ALL_HOTEL = "SELECT * FROM hotel";
  public static final String FIND_ALL_PAYMENT = "SELECT * FROM payment";
  public static final String FIND_ALL_REVIEW = "SELECT * FROM review";

  public static final String FIND_BY_ID_CLIENT = "SELECT * FROM client WHERE idClients=?";
  public static final String FIND_BY_ID_HOTEL = "SELECT * FROM hotel WHERE idHotel=?";
  public static final String FIND_BY_ID_PAYMENT = "SELECT * FROM payment WHERE idPayment=?";
  public static final String FIND_BY_ID_REVIEW = "SELECT * FROM payment WHERE idReview=?";

  private Const(){}
}

package com.epam.connection.runner;

import com.ibatis.common.jdbc.ScriptRunner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;

public class SQLScriptRunner {

  private static final String SQL_SCRIPT_FILE_PATH = "src\\main\\resources\\hotels.sql";
  private static Logger logger = LogManager.getLogger(SQLScriptRunner.class);


  private SQLScriptRunner() {
  }

  public static void runScript(String url, String user, String password) {
    try {
      Connection con = DriverManager.getConnection(url, user, password);
      ScriptRunner sr = new ScriptRunner(con, false, false);
      Reader reader = new BufferedReader(new FileReader(SQL_SCRIPT_FILE_PATH));
      sr.runScript(reader);
    } catch (Exception exception) {
      logger.fatal("Failed to Execute: " + SQL_SCRIPT_FILE_PATH + " The error is: " + exception);
    }
  }
}
package com.epam.connection;

import com.epam.connection.runner.SQLScriptRunner;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Connect {

    private static final Logger logger = LogManager.getLogger(Connect.class);
    private static final String url = "jdbc:mysql://localhost:3306/?serverTimezone=UTC&useSSL=false";
    private static final String user = "root";
    private static final String password = "1234";
    private static Connection connection = null;

    private Connect() {
    }

    public static Connection getConnection() {
        try {
            if (connection == null || connection.isClosed()) {
                connection = DriverManager.getConnection(url, user, password);
                ResultSet resultSet = connection.getMetaData().getCatalogs();
                List<String> databases = new ArrayList<>();
                while (resultSet.next()) {
                    databases.add(resultSet.getString(1));
                }
                if (!databases.contains("hotels")) {
                    SQLScriptRunner.runScript(url, user, password);
                }
            }
        } catch (SQLException e) {
            logger.fatal("SQLException: ");
            logger.fatal("SQLState: ");
            logger.fatal("VendorError: ");
        }
        return connection;
    }

    public static boolean closeConnection() {
        try {
            connection.close();
            return true;
        } catch (SQLException error) {
            logger.error("Enable to close connection" + error);
            return false;
        }
    }
}

package com.epam.transformer;

import com.epam.connection.Connect;
import com.epam.model.annotation.Column;
import com.epam.model.annotation.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer<T> {

  private static Logger logger = LogManager.getLogger(Connect.class);
  private final Class<T> clazz;

  public Transformer(Class<T> clazz) {
    this.clazz = clazz;
  }

  public Object fromResultSetToEntity(ResultSet rs) throws SQLException {
    Object entity = null;
    try {
      entity = clazz.getConstructor().newInstance();
      if (clazz.isAnnotationPresent(Table.class)) {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
          if (field.isAnnotationPresent(Column.class)) {
            Column column = field.getAnnotation(Column.class);
            String name = column.name();
            field.setAccessible(true);
            Class fieldType = field.getType();
            if (fieldType == String.class) {
              field.set(entity, rs.getString(name));
            } else if (fieldType == Integer.class) {
              field.set(entity, rs.getInt(name));
            } else if (fieldType == Date.class) {
              field.set(entity, rs.getDate(name));
            } else if (fieldType == Double.class) {
              field.set(entity, rs.getDouble(name));
            }
          }
        }
      }
    } catch (InstantiationException | IllegalAccessException
        | InvocationTargetException | NoSuchMethodException exception) {
      logger.fatal(exception.getClass().getSimpleName());
    }
    return entity;
  }
}

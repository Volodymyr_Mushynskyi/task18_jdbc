package com.epam.service;

import com.epam.dao.implementation.ClientDaoImplementation;
import com.epam.model.Client;

import java.sql.SQLException;
import java.util.List;

public class ClientService {

  public List<Client> findAll() throws SQLException {
    return new ClientDaoImplementation().findAll();
  }

  public Client findById(Integer id) throws SQLException {
    return new ClientDaoImplementation().findById(id);
  }

  public int create(Client entity) throws SQLException {
    return new ClientDaoImplementation().create(entity);
  }
}

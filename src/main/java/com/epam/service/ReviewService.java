package com.epam.service;

import com.epam.dao.implementation.ReviewDaoImplementation;
import com.epam.model.Review;

import java.sql.SQLException;
import java.util.List;

public class ReviewService {
  public List<Review> findAll() throws SQLException {
    return new ReviewDaoImplementation().findAll();
  }

  public Review findById(Integer id) throws SQLException {
    return new ReviewDaoImplementation().findById(id);
  }
}

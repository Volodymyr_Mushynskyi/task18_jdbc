package com.epam.service;

import com.epam.dao.implementation.RoomDaoImpletation;
import com.epam.model.Room;

import java.sql.SQLException;
import java.util.List;

public class RoomService {
  public List<Room> findAll() throws SQLException {
    return new RoomDaoImpletation().findAll();
  }

  public  Room findById(Integer id) throws SQLException {
    return new RoomDaoImpletation().findById(id);
  }
}

package com.epam.service;

import com.epam.dao.implementation.PaymentDaoImplementation;
import com.epam.model.Payment;

import java.sql.SQLException;
import java.util.List;

public class PaymentService {
  public List<Payment> findAll() throws SQLException {
    return new PaymentDaoImplementation().findAll();
  }

  public Payment findById(Integer id) throws SQLException {
    return new PaymentDaoImplementation().findById(id);
  }
}

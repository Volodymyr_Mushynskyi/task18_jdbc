package com.epam.service;

import com.epam.dao.implementation.HotelDaoImplementation;
import com.epam.model.Hotel;

import java.sql.SQLException;
import java.util.List;

public class HotelService {
  public List<Hotel> findAll() throws SQLException {
    return new HotelDaoImplementation().findAll();
  }

  public  Hotel findById(Integer id) throws SQLException {
    return new HotelDaoImplementation().findById(id);
  }
}

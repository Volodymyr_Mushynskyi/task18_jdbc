package com.epam.dao;

import com.epam.model.Booking;

public interface BookingDao extends OptionDao<Booking,Integer> {
}

package com.epam.dao;

import com.epam.model.Hotel;

public interface HotelDao extends OptionDao<Hotel, Integer> {
}

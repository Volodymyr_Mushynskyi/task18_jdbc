package com.epam.dao;

import com.epam.model.Review;

public interface ReviewDao extends OptionDao<Review, Integer> {

}

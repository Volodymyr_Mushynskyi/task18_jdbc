package com.epam.dao;

import com.epam.model.Room;

public interface RoomDao extends OptionDao<Room, Integer> {
}

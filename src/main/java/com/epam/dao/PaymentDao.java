package com.epam.dao;

import com.epam.model.Payment;

public interface PaymentDao extends OptionDao<Payment, Integer> {
}

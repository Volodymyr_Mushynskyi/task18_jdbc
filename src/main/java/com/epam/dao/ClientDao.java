package com.epam.dao;

import com.epam.model.Client;

public interface ClientDao extends OptionDao<Client, Integer>{
}

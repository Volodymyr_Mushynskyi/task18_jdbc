package com.epam.dao.implementation;

import com.epam.connection.Connect;
import com.epam.dao.RoomDao;
import com.epam.model.Room;
import com.epam.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.constant.Const.*;

public class RoomDaoImpletation implements RoomDao {

  @Override
  public List<Room> findAll() throws SQLException {
    List<Room> rooms = new ArrayList<>();
    Connection connection = Connect.getConnection();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(FIND_ALL_REVIEW)) {
        while (resultSet.next()) {
          rooms.add((Room) new Transformer(Room.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return rooms;
  }

  @Override
  public Room findById(Integer id) throws SQLException {
    Room room = null;
    Connection connection = Connect.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID_REVIEW)) {
      ps.setInt(1, id);
      try (ResultSet resultSet = ps.executeQuery()) {
        if (resultSet.next()) {
          room = (Room) new Transformer(Room.class).fromResultSetToEntity(resultSet);
        }
      }
    }
    return room;
  }

  @Override
  public int create(Room entity) throws SQLException {
    Connection connection = Connect.getConnection();
    try (PreparedStatement statement = connection.prepareStatement(CREATE_REVIEW)) {
      statement.setInt(1, entity.getIdRoom());
      statement.setBoolean(2, entity.getRoomState());
      statement.setDouble(3, entity.getRoomPrice());
      statement.setString(4, entity.getRoomInDate());
      statement.setString(5, entity.getRoomOutDate());
      statement.setString(6, entity.getHotelIdHotel());
      statement.setString(7, entity.getBookingRoomId());
      return statement.executeUpdate();
    }
  }

  @Override
  public int update(Room entity) throws SQLException {
    return 0;
  }

  @Override
  public int delete(Integer integer) throws SQLException {
    return 0;
  }
}

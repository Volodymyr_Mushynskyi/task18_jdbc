package com.epam.dao.implementation;

import com.epam.connection.Connect;
import com.epam.dao.HotelDao;
import com.epam.model.Client;
import com.epam.model.Hotel;
import com.epam.transformer.Transformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.constant.Const.*;

public class HotelDaoImplementation implements HotelDao {
  private static Logger logger = LogManager.getLogger(Connect.class);

  @Override
  public List<Hotel> findAll() throws SQLException {
    List<Hotel> hotels = new ArrayList<>();
    Connection connection = Connect.getConnection();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(FIND_ALL_HOTEL)) {
        while (resultSet.next()) {
          hotels.add((Hotel) new Transformer(Hotel.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return hotels;
  }

  @Override
  public Hotel findById(Integer id) throws SQLException {
    Hotel hotels = null;
    Connection connection = Connect.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID_HOTEL)) {
      ps.setInt(1, id);
      try (ResultSet resultSet = ps.executeQuery()) {
        if (resultSet.next()) {
          hotels = (Hotel) new Transformer(Hotel.class).fromResultSetToEntity(resultSet);
        }
      }
    }
    return hotels;
  }

  @Override
  public int create(Hotel entity) throws SQLException {
    Connection connection = Connect.getConnection();
    try (PreparedStatement statement = connection.prepareStatement(CREATE_HOTEL)) {
      statement.setInt(1, entity.getId());
      statement.setString(2, entity.getHotelName());
      statement.setString(3, entity.getHotelAddress());
      statement.setInt(4, entity.getHotelsRoom());
      statement.setInt(5, entity.getFreeRomm());
      statement.setInt(6, entity.getBookingHotelId());
      return statement.executeUpdate();
    }
  }

  @Override
  public int update(Hotel entity) throws SQLException {
    return 0;
  }

  @Override
  public int delete(Integer integer) throws SQLException {
    return 0;
  }
}

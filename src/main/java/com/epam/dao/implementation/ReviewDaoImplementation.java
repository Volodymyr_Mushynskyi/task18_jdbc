package com.epam.dao.implementation;

import com.epam.connection.Connect;
import com.epam.dao.ReviewDao;
import com.epam.model.Payment;
import com.epam.model.Review;
import com.epam.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.constant.Const.*;

public class ReviewDaoImplementation implements ReviewDao {

  @Override
  public List<Review> findAll() throws SQLException {
    List<Review> reviews = new ArrayList<>();
    Connection connection = Connect.getConnection();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(FIND_ALL_REVIEW)) {
        while (resultSet.next()) {
          reviews.add((Review) new Transformer(Review.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return reviews;
  }

  @Override
  public Review findById(Integer id) throws SQLException {
    Review review = null;
    Connection connection = Connect.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID_REVIEW)) {
      ps.setInt(1, id);
      try (ResultSet resultSet = ps.executeQuery()) {
        if (resultSet.next()) {
          review = (Review) new Transformer(Review.class).fromResultSetToEntity(resultSet);
        }
      }
    }
    return review;
  }

  @Override
  public int create(Review entity) throws SQLException {
    Connection connection = Connect.getConnection();
    try (PreparedStatement statement = connection.prepareStatement(CREATE_REVIEW)) {
      statement.setInt(1, entity.getIdReview());
      statement.setString(2, entity.getDateReview());
      statement.setString(3, entity.getStars());
      statement.setInt(4, entity.getPaymantState());
      return statement.executeUpdate();
    }
  }

  @Override
  public int update(Review entity) throws SQLException {
    return 0;
  }

  @Override
  public int delete(Integer integer) throws SQLException {
    return 0;
  }
}

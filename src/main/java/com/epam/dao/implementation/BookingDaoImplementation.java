package com.epam.dao.implementation;

import com.epam.dao.BookingDao;
import com.epam.model.Booking;

import java.sql.SQLException;
import java.util.List;

public class BookingDaoImplementation implements BookingDao {
  @Override
  public List<Booking> findAll() throws SQLException {
    return null;
  }

  @Override
  public Booking findById(Integer integer) throws SQLException {
    return null;
  }

  @Override
  public int create(Booking entity) throws SQLException {
    return 0;
  }

  @Override
  public int update(Booking entity) throws SQLException {
    return 0;
  }

  @Override
  public int delete(Integer integer) throws SQLException {
    return 0;
  }
}

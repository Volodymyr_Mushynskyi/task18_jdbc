package com.epam.dao.implementation;

import com.epam.connection.Connect;
import com.epam.model.metadata.ColumnMetaData;
import com.epam.model.metadata.ForeignKeyMetaData;
import com.epam.model.metadata.TableMetaData;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MetaDataDaoImplementation {

  public List<String> findAllTable() throws SQLException {
    List<String> tables = new ArrayList<>();
    String[] types = {"TABLE"};
    Connection connection = Connect.getConnection();
    DatabaseMetaData databaseMetaData = connection.getMetaData();
    ResultSet result = databaseMetaData.getTables(connection.getCatalog(), null, "%", types);

    while (result.next()) {
      String tableName = result.getString("NAME_OF_TABLE");
      tables.add(tableName);
    }
    return tables;
  }

  public List<TableMetaData> getTablesStructure() throws SQLException {
    List<TableMetaData> tableMetaDataList = new ArrayList<>();
    Connection connection = Connect.getConnection();
    DatabaseMetaData databaseMetaData = connection.getMetaData();

    String[] types = {"TABLE"};
    String dbName = connection.getCatalog();
    ResultSet result = databaseMetaData.getTables(dbName, null, "%", types);

    while (result.next()) {
      String tableName = result.getString("TABLE_NAME");
      TableMetaData tableMetaData = new TableMetaData();
      tableMetaData.setDbName(dbName);
      tableMetaData.setTableName(tableName);

      List<String> pkList = new ArrayList<>();
      ResultSet PKs = databaseMetaData.getPrimaryKeys(connection.getCatalog(), null, tableName);
      while (PKs.next()) {
        pkList.add(PKs.getString("COLUMN_NAME"));
      }

      List<ColumnMetaData> columnsMetaData = new ArrayList<>();
      ResultSet columnsRS = databaseMetaData.getColumns(dbName, null, tableName, "%");
      while (columnsRS.next()) {
        ColumnMetaData columnMetaData = new ColumnMetaData();
        columnMetaData.setColumnName(columnsRS.getString("COLUMN_NAME"));
        columnMetaData.setDataType(columnsRS.getString("TYPE_NAME"));
        columnMetaData.setColumnSize(columnsRS.getString("COLUMN_SIZE"));
        columnMetaData.setDecimalDigits(columnsRS.getString("DECIMAL_DIGITS"));
        boolean cond = columnsRS.getString("NULL").equals("YES");
        columnMetaData.setNullable(cond);
        cond = columnsRS.getString("IS_AUTOINCREMENT").equals("IS_AUTOINCREMENT");
        columnMetaData.setAutoIncrement(cond);

        columnMetaData.setPrimaryKey(false);
        for (String pkName : pkList) {
          if (columnMetaData.getColumnName().equals(pkName)) {
            columnMetaData.setPrimaryKey(true);
            break;
          }
        }
        columnsMetaData.add(columnMetaData);
      }
      tableMetaData.setColumnMetaData(columnsMetaData);

      List<ForeignKeyMetaData> fkMetaDataList = new ArrayList<>();
      ResultSet FKsRS = databaseMetaData.getImportedKeys(dbName, null, tableName);
      while (FKsRS.next()) {
        ForeignKeyMetaData fk = new ForeignKeyMetaData();
        fk.setFkColumnName(FKsRS.getString("FKCOLUMN_NAME"));
        fk.setPkTableName(FKsRS.getString("PKTABLE_NAME"));
        fk.setPkColumnName(FKsRS.getString("PKCOLUMN_NAME"));
        fkMetaDataList.add(fk);
      }
      tableMetaData.setForeignKeyList(fkMetaDataList);

      tableMetaDataList.add(tableMetaData);
    }
    return tableMetaDataList;
  }
}

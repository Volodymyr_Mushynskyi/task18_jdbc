package com.epam.dao.implementation;

import com.epam.connection.Connect;
import com.epam.dao.PaymentDao;
import com.epam.model.Hotel;
import com.epam.model.Payment;
import com.epam.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.constant.Const.*;

public class PaymentDaoImplementation implements PaymentDao {
  @Override
  public List<Payment> findAll() throws SQLException {
    List<Payment> payments = new ArrayList<>();
    Connection connection = Connect.getConnection();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(FIND_ALL_PAYMENT)) {
        while (resultSet.next()) {
          payments.add((Payment) new Transformer(Payment.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return payments;
  }

  @Override
  public Payment findById(Integer id) throws SQLException {
    Payment payments = null;
    Connection connection = Connect.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID_PAYMENT)) {
      ps.setInt(1, id);
      try (ResultSet resultSet = ps.executeQuery()) {
        if (resultSet.next()) {
          payments = (Payment) new Transformer(Payment.class).fromResultSetToEntity(resultSet);
        }
      }
    }
    return payments;
  }

  @Override
  public int create(Payment entity) throws SQLException {
    Connection connection = Connect.getConnection();
    try (PreparedStatement statement = connection.prepareStatement(CREATE_PAYMENT)) {
      statement.setInt(1, entity.getId());
      statement.setString(2, entity.getDate());
      statement.setDouble(3, entity.getSum());
      statement.setInt(4, entity.getAccount());
      statement.setBoolean(5, entity.getPaymantState());
      statement.setInt(6, entity.getState());
      return statement.executeUpdate();
    }
  }

  @Override
  public int update(Payment entity) throws SQLException {
    return 0;
  }

  @Override
  public int delete(Integer integer) throws SQLException {
    return 0;
  }
}

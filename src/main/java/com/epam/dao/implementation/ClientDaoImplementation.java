package com.epam.dao.implementation;

import com.epam.connection.Connect;
import com.epam.dao.ClientDao;
import com.epam.model.Client;
import com.epam.transformer.Transformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.constant.Const.CREATE_CLIENT;
import static com.epam.constant.Const.FIND_ALL_CLIENTS;
import static com.epam.constant.Const.FIND_BY_ID_CLIENT;

public class ClientDaoImplementation implements ClientDao {

  private static Logger logger = LogManager.getLogger(Connect.class);

  @Override
  public List<Client> findAll() throws SQLException {
    List<Client> clients = new ArrayList<>();
    Connection connection = Connect.getConnection();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(FIND_ALL_CLIENTS)) {
        while (resultSet.next()) {
          clients.add((Client) new Transformer(Client.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return clients;
  }

  @Override
  public Client findById(Integer id) throws SQLException {
    Client client = null;
    Connection connection = Connect.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID_CLIENT)) {
      ps.setInt(1, id);
      try (ResultSet resultSet = ps.executeQuery()) {
        if (resultSet.next()) {
          client = (Client) new Transformer(Client.class).fromResultSetToEntity(resultSet);
        }
      }
    }
    return client;
  }

  @Override
  public int create(Client entity) throws SQLException {
      Connection connection = Connect.getConnection();
      try (PreparedStatement statement = connection.prepareStatement(CREATE_CLIENT)) {
        statement.setInt(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getSurname());
        statement.setString(4, entity.getEmail());
        statement.setInt(5, entity.getAccount());
        statement.setString(6, entity.getState());
        return statement.executeUpdate();
      }
  }

  @Override
  public int update(Client entity) throws SQLException {
    return 0;
  }

  @Override
  public int delete(Integer integer) throws SQLException {
    return 0;
  }
}

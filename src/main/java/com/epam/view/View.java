package com.epam.view;

import com.epam.connection.Connect;
import com.epam.model.Client;
import com.epam.model.Hotel;
import com.epam.model.Payment;
import com.epam.model.Review;
import com.epam.service.ClientService;
import com.epam.service.HotelService;
import com.epam.service.PaymentService;
import com.epam.service.ReviewService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Logger logger = LogManager.getLogger(View.class);
    private static Scanner input = new Scanner(System.in);

    public View() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - find Client by ID");
        menu.put("2", "2 - find Client All");
        menu.put("3", "3 - create Client");

        menu.put("4", "4 - find Hotel by ID");
        menu.put("5", "5 - find Hotel All");
        menu.put("6", "6 - create Hotel ");

        menu.put("7", "7 - find Payment by ID");
        menu.put("8", "8 - find Payment All");

        menu.put("9", "9 - find Review by ID");
        menu.put("10", "10 - find Review All");
        menu.put("11", "11 - create Review ");

        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", this::selectClientById);
        methodsMenu.put("2", this::selectAllClient);
        methodsMenu.put("3", this::selectAllClient);

        methodsMenu.put("4", this::selectHotelById);
        methodsMenu.put("5", this::selectAllHotel);

        methodsMenu.put("7", this::selectPaymentById);
        methodsMenu.put("8", this::selectAllPayment);

        methodsMenu.put("9", this::selectReviewById);
        methodsMenu.put("10", this::selectAllReview);
    }

    private void selectClientById() throws SQLException {
        logger.info("Enter client ID: ");
        Integer id = input.nextInt();
        Client client = new ClientService().findById(id);
        System.out.println(client);
    }

    private void selectAllClient() throws SQLException {
        logger.trace("Clients");
        List<Client> clients = new ClientService().findAll();
        for (Client client : clients) {
            logger.trace(client);
        }
    }

    private void createClient() throws SQLException {
        System.out.print("Enter ID: ");
        Integer id = input.nextInt();
        System.out.print("Enter client Name: ");
        String name = input.nextLine();
        System.out.print("Enter client SureName: ");
        String sureName = input.nextLine();
        System.out.print("Enter email: ");
        String email = input.nextLine();
        System.out.print("Enter Account: ");
        Integer account = input.nextInt();
        System.out.print("Enter Client State: ");
        String clientState = input.nextLine();

        Client entity = new Client(id, name, sureName, email, account, clientState);
        input.nextLine();

        int count = new ClientService().create(entity);
        System.out.printf("Created %d rows\n", count);
    }

    private void selectHotelById() throws SQLException {
        logger.info("Enter hotel ID: ");
        Integer id = input.nextInt();
        Hotel hotel = new HotelService().findById(id);
        System.out.println(hotel);
    }

    private void selectAllHotel() throws SQLException {
        logger.trace("Hotels");
        List<Hotel> hotels = new HotelService().findAll();
        for (Hotel hotel : hotels) {
            logger.trace(hotel);
        }
    }

    private void selectPaymentById() throws SQLException {
        logger.info("Enter payment ID: ");
        Integer id = input.nextInt();
        Payment payment = new PaymentService().findById(id);
        System.out.println(payment);
    }

    private void selectAllPayment() throws SQLException {
        logger.trace("Payments");
        List<Payment> payments = new PaymentService().findAll();
        for (Payment payment : payments) {
            logger.trace(payment);
        }
    }

    private void selectReviewById() throws SQLException {
        logger.info("Enter reviews ID: ");
        Integer id = input.nextInt();
        Review review = new ReviewService().findById(id);
        System.out.println(review);
    }

    private void selectAllReview() throws SQLException {
        logger.trace("Reviews");
        List<Review> reviews = new ReviewService().findAll();
        for (Review review : reviews) {
            logger.trace(review);
        }
    }

    public void showMenu() {
        String keyMenu;
        do {
            printMenuAction();
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("0"));
        Connect.closeConnection();
    }

    private void printMenuAction() {
        System.out.println("--------------MENU-----------\n");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}
